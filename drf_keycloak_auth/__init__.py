"""Metadata for drf_keycloak_auth package."""
__title__ = 'drf_keycloak_auth'
__description__ = \
    'A convenience libary for authenticating users from Keycloak access tokens'
__url__ = 'https://gitlab.com/ecocommons-australia/lib/drf-keycloak-auth'
__version__ = '0.0.1.dev10'
__author__ = 'EcoCommons Australia, Gary Burgmann'
__author_email__ = 'ecocommons@griffith.edu.au, garyburgmann@gmail.com'
__license__ = 'MIT'
